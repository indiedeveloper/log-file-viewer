<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function view(Request $request)
    {
        $path = $request->get('path');
        try {
            if (substr(mime_content_type($path), 0, 5) === "text/") {
                $file = new \SplFileObject($path);
                $file->seek(0);
                $lines = array();
                $count = 1;
                while (!$file->eof()) {
                    $lines[$count] = $file->fgets();
                    $count++;
                    if ($count > 10)
                        break;
                }
                session([
                    'hasPath' => true,
                    'path' => $path
                ]);
                if ($file->eof()) {
                    session(['next' => null]);
                } else {
                    session(['next' => $count - 1]);
                }
                return response()->json($lines);
            } else {
                return response()
                    ->json(['code' => 404, 'message' => 'It is not a log file.'], 404);
            }
        } catch (\ErrorException $e) {
            return response()->json(
                ['code' => 404, 'message' => 'File not found.'], 404);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function next()
    {
        return $this->handleValidation(function ($path) {
            $next = session('next');
            if (!is_null($next)) {
                $file = new \SplFileObject($path);
                $file->seek($next);
                $lines = array();
                $count = $next + 1;
                $lines[$count] = $file->current();
                while (!$file->eof()) {
                    $count++;
                    if ($count > $next + 10)
                        break;
                    $lines[$count] = $file->fgets();
                }
                $file->next();
                if ($file->eof()) {
                    session(['next' => null]);
                } else {
                    session(['next' => $count - 1]);
                }
                session(['previous' => $next - 10]);
                return response()->json($lines);
            } else {
                return response()->json(['code' => 500,
                    'message' => 'You have reached the last page of the file'],
                    500);
            }
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function previous() {
        return $this->handleValidation(function ($path) {
            $previous = session('previous');
            if (!is_null($previous)) {
                $file = new \SplFileObject($path);
                $file->seek($previous);
                $lines = array();
                $count = $previous + 1;
                $lines[$count] = $file->current();
                while (!$file->eof()) {
                    $count++;
                    if ($count > $previous + 10)
                        break;
                    $lines[$count] = $file->fgets();
                }
                if ($previous < 10) {
                    session(['previous' => null]);
                } else {
                    session(['previous' => $previous - 10]);
                }
                session(['next' => session('previous') + 20]);
                return response()->json($lines);
            } else {
                return response()->json(['code' => 500,
                    'message' => 'You have reached the first page of the file'],
                    500);
            }
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function first() {
        return $this->handleValidation(function ($path) {
            $file = new \SplFileObject($path);
            $file->seek(0);
            $lines = array();
            $count = 1;
            while (!$file->eof()) {
                $lines[$count] = $file->fgets();
                $count++;
                if ($count > 10)
                    break;
            }
            session(['previous' => null]);
            if ($file->eof()) {
                session(['next' => null]);
            } else {
                session(['next' => $count - 1]);
            }
            return response()->json($lines);
        });
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function last() {
        return $this->handleValidation(function ($path) {
            $file = new \SplFileObject($path);
            $file->seek(PHP_INT_MAX);
            $totaLines = $file->key() + 1;
            $index = intdiv($totaLines, 10) * 10;
            $file->seek($index - 1);
            $lines = array();
            $count = $index + 1;
            $lines[$count] = $file->current();
            while (!$file->eof()) {
                $lines[$count] = $file->fgets();
                $count++;
            }
            session(['next' => null]);
            if ($index < 10) {
                session(['previous' => null]);
            } else {
                session(['previous' => $index - 10]);
            }
            return response()->json($lines);
        });
    }

    /**
     * @param $task
     * @return \Illuminate\Http\JsonResponse
     */
    private function handleValidation($task) {
        if (session('hasPath')) {
            $path = session('path');
            try {
                if (substr(mime_content_type($path), 0, 5) === "text/") {
                    return $task($path);
                } else {
                    return response()
                        ->json(['code' => 404, 'message' => 'It is not a log file.'], 404);
                }
            } catch (\ErrorException $e) {
                return response()->json(
                    ['code' => 404, 'message' => 'File not found.'], 404);
            }
        } else {
            return response()->json(['code' => 500,
                'message' => 'Please input path to the file.'], 500);
        }
    }
}