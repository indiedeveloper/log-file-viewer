$(document).ready(function () {
    $('#view').click(function () {
        var path = $('#path').val();
        if (path == "") {
            $('.container').prepend('<div class="alert alert-danger col-xs-7 top1" role="alert">Path to the file is required.</div>');
        } else {
            $.post("view", {
                path: path,
                _token: window.Laravel.csrfToken
            }).done(function (data) {
                populate_table(data)
            }).fail(function (data) {
                handle_fail(data)
            });
        }
    });

    $('#next').click(function () {
        handle_click('next');
    });

    $('#previous').click(function () {
        handle_click('previous');
    });

    $('#first').click(function () {
        handle_click('first');
    });

    $('#last').click(function () {
        handle_click('last');
    });
});

/**
 * @param table
 * @param data
 */
function populate_table(data) {
    var table = $('#table');
    table.html("");
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            table.append('<tr><td class="col-xs-1 column">' + key + '</td><td>' + data[key] + '</td></tr>');
        }
    }
}

/**
 * @param action
 */
function handle_click(action) {
    $.post(action, {
        _token: window.Laravel.csrfToken
    }).done(function (data) {
        populate_table(data)
    }).fail(function (data) {
        handle_fail(data);
    });
}
function handle_fail(data) {
    data = JSON.parse(data.responseText);
    $('.alert').remove();
    if (data['code'] == 404) {
        $('.container').prepend('<div class="alert alert-danger col-xs-7 top1" role="alert">' + data['message'] + '</div>');
    } else if (data['code'] == 500) {
        $('.container').prepend('<div class="alert alert-warning col-xs-7 top1" role="alert">' + data['message'] + '</div>');
    }
    $('.alert').delay(1000).fadeOut(800);
}