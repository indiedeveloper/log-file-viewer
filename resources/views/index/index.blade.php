<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <style>
        .top8 { margin-top:8%; }
        .top3 { margin-top:3%; }
        .top1 { margin-top:1%; position: absolute; }
        .column {background-color: #edf3ff}
        td {
            border-top: none !important;
            border-bottom: none !important;
            padding: 8px !important;
        }
        li {
            padding: 0px !important;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.2.0.min.js"
            integrity="sha256-JAW99MJVpJBGcbzEuXk4Az05s/XyDdBomFqNlM3ic+I="
            crossorigin="anonymous"></script>
    <script src="{{asset('resources/assets/js/app.js')}}"></script>
    <script>
        window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
    </script>

    <title>Log File Viewer</title>
</head>
<body>
<div class="container">
    <div class="row input-group top8">
        <input type="text" class="form-control col-xs-9" placeholder="/path/to/file" id="path">
        <span class="input-group-btn">
            <button class="btn btn-default" id="view" type="button">View</button>
        </span>
    </div>
    <div class="row top3">
        <table class="table table-bordered" id="table"></table>
    </div>
    <div class="row">
        <nav aria-label="Page navigation">
            <ul class="pagination pagination-lg col-xs-12">
                <li class="col-xs-3">
                    <a href="#" aria-label="First" class="col-xs-12 text-center" id="first">
                        <span aria-hidden="true">&mid;&lt;</span>
                    </a>
                </li>
                <li class="col-xs-3">
                    <a href="#" aria-label="Previous" class="col-xs-12 text-center" id="previous">
                        <span aria-hidden="true">&lt;</span>
                    </a>
                </li>
                <li class="col-xs-3">
                    <a href="#" aria-label="Next" class="col-xs-12 text-center" id="next">
                        <span aria-hidden="true">&gt;</span>
                    </a>
                </li>
                <li class="col-xs-3">
                    <a href="#" aria-label="Last" class="col-xs-12 text-center" id="last">
                        <span aria-hidden="true">&gt;&mid;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
</body>
</html>