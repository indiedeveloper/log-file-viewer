<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::post('/view', 'IndexController@view');
Route::post('/next', 'IndexController@next');
Route::post('/previous', 'IndexController@previous');
Route::post('/first', 'IndexController@first');
Route::post('/last', 'IndexController@last');
