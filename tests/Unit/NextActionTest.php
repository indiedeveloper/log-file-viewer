<?php

namespace Tests\Unit;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

class NextActionTest extends TestCase
{
    /**
     * @var  vfsStreamDirectory
     */
    private $validFile;
    private $notAFile;
    private $directory;

    /**
     * set up test environmemt
     */
    public function setUp()
    {
        parent::setUp();
        vfsStream::setup('log')->url();
        $this->validFile = vfsStream::url('log/test.log');
        $this->notAFile = "dsfsdfs";
        $file = new \SplFileObject($this->validFile, "w");
        for ($i = 1; $i < 33; $i++)
            $file->fwrite("This is line " . $i . "\n");
        if (!file_exists(dirname(__FILE__) . '/test_directory') === true) {
            mkdir(dirname(__FILE__) . '/test_directory');
        }
        $this->directory = dirname(__FILE__) . '/test_directory';
    }

    public function testDirectoryGotError()
    {
        $response = $this->postJson('/view', ['path' => $this->directory]);
        $response->assertStatus(404);
        $response->assertExactJson(['code' => 404, 'message' => 'It is not a log file.']);
    }

    public function testNotAFileGotError()
    {
        $response = $this->postJson('/view', ['path' => $this->notAFile]);
        $response->assertStatus(404);
        $response->assertExactJson(['code' => 404, 'message' => 'File not found.']);
    }

    public function testHasPathMustBeSet()
    {
        $this->session([
            'path' => $this->validFile,
            'next' => 10
        ]);
        $response = $this->postJson('/next');
        $response->assertStatus(500);
        $response->assertExactJson(['code' => 500,
            'message' => 'Please input path to the file.']);
    }

    public function testValidFileGotSuccess()
    {
        $this->session([
            'hasPath' => true,
            'path' => $this->validFile,
            'next' => 10
        ]);
        $response = $this->postJson('/next');
        $response->assertStatus(200);
        $data = [
            11 => "This is line 11\n",
            12 => "This is line 12\n",
            13 => "This is line 13\n",
            14 => "This is line 14\n",
            15 => "This is line 15\n",
            16 => "This is line 16\n",
            17 => "This is line 17\n",
            18 => "This is line 18\n",
            19 => "This is line 19\n",
            20 => "This is line 20\n"
        ];
        $response->assertExactJson($data);
        $response->assertSessionHas('hasPath', true);
        $response->assertSessionHas('path', $this->validFile);
        $response->assertSessionHas('previous', 0);
        $response->assertSessionHas('next', 20);
        $this->postJson('/next');
        $response = $this->postJson('/next');
        $response->assertSessionMissing('next');
        $response = $this->postJson('/next');
        $response->assertStatus(500);
        $response->assertExactJson(['code' => 500,
            'message' => 'You have reached the last page of the file']);
    }
}
