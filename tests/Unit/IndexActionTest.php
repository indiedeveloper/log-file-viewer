<?php

namespace Tests\Unit;

use Tests\TestCase;

class IndexActionTest extends TestCase
{
    public function testLandingPageLoaded()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('<input type="text" class="form-control col-xs-9" placeholder="/path/to/file" id="path">');
        $response->assertSee('<button class="btn btn-default" id="view" type="button">View</button>');
        $response->assertSee('<table class="table table-bordered" id="table"></table>');
        $response->assertSee('<a href="#" aria-label="First" class="col-xs-12 text-center" id="first">
                        <span aria-hidden="true">&mid;&lt;</span>
                    </a>');
        $response->assertSee('<a href="#" aria-label="Previous" class="col-xs-12 text-center" id="previous">
                        <span aria-hidden="true">&lt;</span>
                    </a>');
        $response->assertSee('<a href="#" aria-label="Next" class="col-xs-12 text-center" id="next">
                        <span aria-hidden="true">&gt;</span>
                    </a>');
        $response->assertSee('<a href="#" aria-label="Last" class="col-xs-12 text-center" id="last">
                        <span aria-hidden="true">&gt;&mid;</span>
                    </a>');
    }
}
