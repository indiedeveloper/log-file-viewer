<?php

namespace Tests\Unit;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

class ViewActionTest extends TestCase
{
    /**
     * @var  vfsStreamDirectory
     */
    private $validFile;
    private $notAFile;
    private $directory;

    /**
     * set up test environmemt
     */
    public function setUp()
    {
        parent::setUp();
        vfsStream::setup('log')->url();
        $this->validFile = vfsStream::url('log/test.log');
        $this->notAFile = "dsfsdfs";
        $file = new \SplFileObject($this->validFile, "w");
        for ($i = 1; $i < 33; $i++)
            $file->fwrite("This is line " . $i . "\n");
        if (!file_exists(dirname(__FILE__) . '/test_directory') === true) {
            mkdir(dirname(__FILE__) . '/test_directory');
        }
        $this->directory = dirname(__FILE__) . '/test_directory';
    }

    public function testDirectoryGotError()
    {
        $response = $this->postJson('/view', ['path' => $this->directory]);
        $response->assertStatus(404);
        $response->assertExactJson(['code' => 404, 'message' => 'It is not a log file.']);
    }

    public function testNotAFileGotError()
    {
        $response = $this->postJson('/view', ['path' => $this->notAFile]);
        $response->assertStatus(404);
        $response->assertExactJson(['code' => 404, 'message' => 'File not found.']);
    }

    public function testValidFileGotSuccess()
    {
        $response = $this->postJson('/view', ['path' => $this->validFile]);
        $response->assertStatus(200);
        $data = [
            1 => "This is line 1\n",
            2 => "This is line 2\n",
            3 => "This is line 3\n",
            4 => "This is line 4\n",
            5 => "This is line 5\n",
            6 => "This is line 6\n",
            7 => "This is line 7\n",
            8 => "This is line 8\n",
            9 => "This is line 9\n",
            10 => "This is line 10\n"
        ];
        $response->assertExactJson($data);
        $response->assertSessionHas('hasPath', true);
        $response->assertSessionHas('path', $this->validFile);
        $response->assertSessionMissing('previous');
        $response->assertSessionHas('next', 10);
    }
}
