<?php

namespace Tests\Unit;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

class LastActionTest extends TestCase
{
    /**
     * @var  vfsStreamDirectory
     */
    private $validFile;
    private $notAFile;
    private $directory;

    /**
     * set up test environmemt
     */
    public function setUp()
    {
        parent::setUp();
        vfsStream::setup('log')->url();
        $this->validFile = vfsStream::url('log/test.log');
        $this->notAFile = "dsfsdfs";
        $file = new \SplFileObject($this->validFile, "w");
        for ($i = 1; $i < 33; $i++)
            $file->fwrite("This is line " . $i . "\n");
        if (!file_exists(dirname(__FILE__) . '/test_directory') === true) {
            mkdir(dirname(__FILE__) . '/test_directory');
        }
        $this->directory = dirname(__FILE__) . '/test_directory';
    }

    public function testDirectoryGotError()
    {
        $response = $this->postJson('/view', ['path' => $this->directory]);
        $response->assertStatus(404);
        $response->assertExactJson(['code' => 404, 'message' => 'It is not a log file.']);
    }

    public function testNotAFileGotError()
    {
        $response = $this->postJson('/view', ['path' => $this->notAFile]);
        $response->assertStatus(404);
        $response->assertExactJson(['code' => 404, 'message' => 'File not found.']);
    }

    public function testHasPathMustBeSet()
    {
        $this->session([
            'path' => $this->validFile
        ]);
        $response = $this->postJson('/first');
        $response->assertStatus(500);
        $response->assertExactJson(['code' => 500,
            'message' => 'Please input path to the file.']);
    }

    public function testValidFileGotSuccess()
    {
        $this->session([
            'hasPath' => true,
            'path' => $this->validFile
        ]);
        $response = $this->postJson('/last');
        $response->assertStatus(200);
        $data = [
            31 => "This is line 31\n",
            32 => "This is line 32\n"
        ];
        $response->assertExactJson($data);
        $response->assertSessionHas('hasPath', true);
        $response->assertSessionHas('path', $this->validFile);
        $response->assertSessionMissing('next');
        $response->assertSessionHas('previous', 20);
    }
}
