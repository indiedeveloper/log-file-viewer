<?php

namespace Tests\Unit;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

class PreviousActionTest extends TestCase
{
    /**
     * @var  vfsStreamDirectory
     */
    private $validFile;
    private $notAFile;
    private $directory;

    /**
     * set up test environmemt
     */
    public function setUp()
    {
        parent::setUp();
        vfsStream::setup('log')->url();
        $this->validFile = vfsStream::url('log/test.log');
        $this->notAFile = "dsfsdfs";
        $file = new \SplFileObject($this->validFile, "w");
        for ($i = 1; $i < 33; $i++)
            $file->fwrite("This is line " . $i . "\n");
        if (!file_exists(dirname(__FILE__) . '/test_directory') === true) {
            mkdir(dirname(__FILE__) . '/test_directory');
        }
        $this->directory = dirname(__FILE__) . '/test_directory';
    }

    public function testDirectoryGotError()
    {
        $response = $this->postJson('/view', ['path' => $this->directory]);
        $response->assertStatus(404);
        $response->assertExactJson(['code' => 404, 'message' => 'It is not a log file.']);
    }

    public function testNotAFileGotError()
    {
        $response = $this->postJson('/view', ['path' => $this->notAFile]);
        $response->assertStatus(404);
        $response->assertExactJson(['code' => 404, 'message' => 'File not found.']);
    }

    public function testHasPathMustBeSet()
    {
        $this->session([
            'path' => $this->validFile,
            'previous' => 20
        ]);
        $response = $this->postJson('/previous');
        $response->assertStatus(500);
        $response->assertExactJson(['code' => 500,
            'message' => 'Please input path to the file.']);
    }

    public function testValidFileGotSuccess()
    {
        $this->session([
            'hasPath' => true,
            'path' => $this->validFile,
            'previous' => 20
        ]);
        $response = $this->postJson('/previous');
        $response->assertStatus(200);
        $data = [
            21 => "This is line 21\n",
            22 => "This is line 22\n",
            23 => "This is line 23\n",
            24 => "This is line 24\n",
            25 => "This is line 25\n",
            26 => "This is line 26\n",
            27 => "This is line 27\n",
            28 => "This is line 28\n",
            29 => "This is line 29\n",
            30 => "This is line 30\n"
        ];
        $response->assertExactJson($data);
        $response->assertSessionHas('hasPath', true);
        $response->assertSessionHas('path', $this->validFile);
        $response->assertSessionHas('previous', 10);
        $response->assertSessionHas('next', 30);
        $this->postJson('/previous');
        $response = $this->postJson('/previous');
        $response->assertSessionMissing('previous');
        $response = $this->postJson('/previous');
        $response->assertStatus(500);
        $response->assertExactJson(['code' => 500,
            'message' => 'You have reached the first page of the file']);
    }
}
